package Modelo;

public class Modelo {
    private int moneda50=0;
    private int moneda100=0;
    private int moneda200=0;
    private int moneda500=0;
    private int moneda1000=0;
    private int total=0;
    private int total2=0;
    private int total3=0;
    private int total31=0;
    private int total32=0;
    private int total33=0;
    private int total34=0;
    private int total4=0;
    private int total41=0;
    private int total42=0;
    private int total43=0;
    private int total44=0;

    public int getMoneda50() {
        return moneda50;
    }

    public void setMoneda50(int moneda50) {
        this.moneda50 = moneda50;
    }

    public int getMoneda100() {
        return moneda100;
    }

    public void setMoneda100(int moneda100) {
        this.moneda100 = moneda100;
    }

    public int getMoneda200() {
        return moneda200;
    }

    public void setMoneda200(int moneda200) {
        this.moneda200 = moneda200;
    }

    public int getMoneda500() {
        return moneda500;
    }

    public void setMoneda500(int moneda500) {
        this.moneda500 = moneda500;
    }

    public int getMoneda1000() {
        return moneda1000;
    }

    public void setMoneda1000(int moneda1000) {
        this.moneda1000 = moneda1000;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal2() {
        return total2;
    }

    public void setTotal2(int total2) {
        this.total2 = total2;
    }

    public int getTotal3() {
        return total3;
    }

    public void setTotal3(int total3) {
        this.total3 = total3;
    }

    public int getTotal31() {
        return total31;
    }

    public void setTotal31(int total31) {
        this.total31 = total31;
    }

    public int getTotal32() {
        return total32;
    }

    public void setTotal32(int total32) {
        this.total32 = total32;
    }

    public int getTotal33() {
        return total33;
    }

    public void setTotal33(int total33) {
        this.total33 = total33;
    }

    public int getTotal34() {
        return total34;
    }

    public void setTotal34(int total34) {
        this.total34 = total34;
    }

    public int getTotal4() {
        return total4;
    }

    public void setTotal4(int total4) {
        this.total4 = total4;
    }

    public int getTotal41() {
        return total41;
    }

    public void setTotal41(int total41) {
        this.total41 = total41;
    }

    public int getTotal42() {
        return total42;
    }

    public void setTotal42(int total42) {
        this.total42 = total42;
    }

    public int getTotal43() {
        return total43;
    }

    public void setTotal43(int total43) {
        this.total43 = total43;
    }

    public int getTotal44() {
        return total44;
    }

    public void setTotal44(int total44) {
        this.total44 = total44;
    }

   public int sumatodaslasmonedas(){
       
       this.total = this.moneda50+this.moneda100+this.moneda200+this.moneda500+this.moneda1000;
       return this.total;
   }
  public int sumadinero(){
       
       this.total2 = this.moneda50*50 + this.moneda100*100 +this.moneda200*200 +this.moneda500*500 +this.moneda1000*1000;
       return this.total2;
   }
   public int sumadenomina(){
       this.total3 = this.moneda50++ ;
       return this.total3;
   }
   public int sumadenomina1(){
       this.total31 = this.moneda100++ ;
       return this.total31;
   }
   public int sumadenomina2(){
       this.total32 = this.moneda200++ ;
       return this.total32;
   }
   public int sumadenomina3(){
       this.total33 = this.moneda500++ ;
       return this.total33;
   }
   public int sumadenomina4(){
       this.total34 = this.moneda1000++ ;
       return this.total34;
   }
     public int sumadinerodenomina(){
       
       this.total4 = this.total3*50;
       return this.total4;
   }
      public int sumadinerodenomina1(){
       
       this.total41 = this.total31*100;
       return this.total41;
   }
       public int sumadinerodenomina2(){
       
       this.total42 = this.total32*200;
       return this.total42;
   }
        public int sumadinerodenomina3(){
       
       this.total43 = this.total33*500;
       return this.total43;
   }
         public int sumadinerodenomina4(){
       
       this.total44 = this.total34*1000;
       return this.total44;
   }
    }
    
