
package controlador;

import Modelo.Modelo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import vista.Vista1;

public class Controlador implements ActionListener{
    private Vista1 view;
    private Modelo model;
    
    public Controlador(Vista1 view,Modelo model)
    {
        this.view = view;
        this.model= model;
        this.view.jButton1.addActionListener(this);
    }
    public void iniciar(){
        view.setTitle("Alcancia");
        view.setLocationRelativeTo(null);
    }
    public void actionPerformed(ActionEvent e){
    model.setMoneda50(Integer.parseInt(view.jTextField1.getText()));
    model.setMoneda100(Integer.parseInt(view.jTextField2.getText()));
    model.setMoneda200(Integer.parseInt(view.jTextField3.getText()));
    model.setMoneda500(Integer.parseInt(view.jTextField4.getText()));
    model.setMoneda1000(Integer.parseInt(view.jTextField5.getText()));
    model.sumatodaslasmonedas();
    model.sumadinero();
    model.sumadenomina();
    model.sumadenomina1();
    model.sumadenomina2();
    model.sumadenomina3();
    model.sumadenomina4();
    ///////////////////////
    model.sumadinerodenomina();
    model.sumadinerodenomina1();
    model.sumadinerodenomina2();
    model.sumadinerodenomina3();
    model.sumadinerodenomina4();
    ////////////////////////////
    view.jTextField6.setText(String.valueOf(model.getTotal()));
    view.jTextField7.setText(String.valueOf(model.getTotal2()));
    /////////////////////////////////////////////////////////
    view.jTextField8.setText(String.valueOf(model.getTotal3()));
    view.jTextField10.setText(String.valueOf(model.getTotal31()));
    view.jTextField11.setText(String.valueOf(model.getTotal32()));
    view.jTextField12.setText(String.valueOf(model.getTotal33()));
    view.jTextField13.setText(String.valueOf(model.getTotal34()));
    //////////////////////////////////////////////////////////
    view.jTextField9.setText(String.valueOf(model.getTotal4()));
    view.jTextField14.setText(String.valueOf(model.getTotal41()));
    view.jTextField15.setText(String.valueOf(model.getTotal42()));
    view.jTextField16.setText(String.valueOf(model.getTotal43()));
    view.jTextField17.setText(String.valueOf(model.getTotal44()));
    }
}